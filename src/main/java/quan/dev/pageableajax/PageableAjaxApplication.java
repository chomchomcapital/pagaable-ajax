package quan.dev.pageableajax;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import quan.dev.pageableajax.entity.User;
import quan.dev.pageableajax.repo.UserRepo;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class PageableAjaxApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PageableAjaxApplication.class, args);
    }

    @Autowired
    UserRepo userRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    public static List<String> roles = Arrays.asList("admin" , "mod" , "user");

    @Override
    public void run(String... args) throws Exception {
        Faker faker = new Faker();
        for (int i = 1; i <= 200; i++) {
            User user = new User()
                    .setId((long) i)
                    .setName(faker.funnyName().name())
                    .setCompany(faker.company().name())
                    .setAge(faker.random().nextInt(80))
                    .setStatus(faker.random().nextBoolean())
                    .setUsername("user"+i)
                    .setEmail(faker.internet().emailAddress())
                    .setPassword(passwordEncoder.encode("123"))
                    .setRole(roles.get(faker.random().nextInt(0 , 2)));
            userRepo.save(user);
        }
        System.out.println("-----------------------init data done");
    }
}
