package quan.dev.pageableajax.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import quan.dev.pageableajax.payload.SearchData;
import quan.dev.pageableajax.repo.UserRepo;

import javax.validation.Valid;

@Controller
@CrossOrigin("*")
public class TestController {

    @Autowired
    UserRepo userRepo;

    @GetMapping("/web")
    public String index() {
        return "index";
    }

    @GetMapping("/getData")
    public ResponseEntity getData(@Valid  SearchData searchData) throws InterruptedException {
        Thread.sleep(1000);
        return ResponseEntity.ok(userRepo.findAll(PageRequest.of(searchData.getPageNumber()-1, searchData.getPageSize())));

    }

    @GetMapping("/web/getData")
    public ResponseEntity getData2(@Valid  SearchData searchData) throws InterruptedException {
        Thread.sleep(1000);
        return ResponseEntity.ok(userRepo.findAll(PageRequest.of(searchData.getPageNumber()-1, searchData.getPageSize())));

    }


}
