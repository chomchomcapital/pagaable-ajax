package quan.dev.pageableajax.payload;

import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {

    private String token;
    private String refreshToken;
    private final String type = "Bearer";
    private Long id;
    private String username;
    private String email;
    private List<String> roles;


    public JwtResponse(String token, String refreshToken, Long id, String username, String email, List<String> roles) {
        this.token = token;
        this.refreshToken = refreshToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }
}
