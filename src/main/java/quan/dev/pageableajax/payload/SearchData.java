package quan.dev.pageableajax.payload;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
public class SearchData {

    @Min(0)
    private int pageNumber;


    @Min(1)
    @Max(20)
    private int pageSize;


}
