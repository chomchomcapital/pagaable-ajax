package quan.dev.pageableajax.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import quan.dev.pageableajax.entity.User;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

}
